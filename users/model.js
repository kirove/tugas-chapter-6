const userList = [];
const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

class UserModel {
  getAllUser = async () => {
    return await db.pengguna.findAll();
  };

  isUserRegistered = async (dataRequest) => {
    const existData = await db.pengguna.findOne({
      where: {
        [Op.or]: [
          { username: dataRequest.username },
          { email: dataRequest.email },
        ],
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  recordNewData = (dataRequest) => {
    db.pengguna.create({
      username: dataRequest.username,
      email: dataRequest.email,
      password: md5(dataRequest.password),
    });
  };

  // verifyLogin = async (dataRequest) => {
  //   const dataUser = await db.pengguna.findOne({
  //     where: {
  //       [Op.or]: [
  //         {
  //           username: dataRequest.username,
  //           // password: md5(password),
  //         },
  //         {
  //           email: dataRequest.email,
  //           // password: md5(password),
  //         },
  //         // { email: email, password: md5(password) },
  //       ],
  //     },
  //   });

  verifyLogin = async (username, password) => {
    const dataUser = await db.pengguna.findOne({
      where: {
        username: username,
        password: md5(password),
      },
    });

    // // const dataUser = userList.find((data) => {
    //   return data.username === username && data.password === md5(password);
    // });
    return dataUser;
  };
}

module.exports = new UserModel();
