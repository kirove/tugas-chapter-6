const UserModel = require("./model");

// Untuk mendapatkan semua user yang tersimpan
class userController {
  getAllUser = async (req, res) => {
    const allUsers = await UserModel.getAllUser();
    return res.json(allUsers);
  };

  // Untuk pengisian data-data User
  registeredUser = async (req, res) => {
    const dataRequest = req.body;

    if (dataRequest.username === undefined || dataRequest.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username jangan lupa diisi donk :(" });
    }

    if (dataRequest.email === undefined || dataRequest.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email jangan lupa diisi donk :(" });
    }

    if (dataRequest.password === undefined || dataRequest.password === "") {
      res.statusCode = 400;
      return res.json({ message: "Password jangan lupa diisi donk :(" });
    }

    const existData = await UserModel.isUserRegistered(dataRequest);

    if (existData) {
      res.statusCode = 400;
      return res.json({
        message:
          "Username atau email sudah pernah terdaftar. Ganti yang lain ya :)",
      });
    }

    UserModel.recordNewData(dataRequest);
    return res.json({ message: "Oke deh, sudah tersimpan ^_^" });
  };

  // Untuk login

  // userLogin = async (req, res) => {
  //   // const { username, password } = req.body;
  //   // const dataLogin = await db.pengguna.findOne()
  //   const dataLogin = await UserModel.verifyLogin();
  //   if (dataLogin) {
  //     return res.json(dataLogin);
  //   } else {
  //     return res.json({ message: "Uppsss salah >_<" });
  //   }
  // };

  userLogin = async (req, res) => {
    const { username, password } = req.body;
    // const dataLogin = await db.pengguna.findOne()
    const dataLogin = await UserModel.verifyLogin(username, password);
    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      return res.json({ message: "Uppsss salah >_<" });
    }
  };
}

module.exports = new userController();
